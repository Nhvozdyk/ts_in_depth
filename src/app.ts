import {Category} from "./enums";
import {logSearchResult} from "./functions";

showHello('greeting', 'TypeScript');

function showHello(divName: string, name: string) {
    const elt = document.getElementById(divName);
    elt.innerText = `Hello from ${name}`;
}


// type Books = ReadonlyArray<Book>


//
// let myBooks: Book  = {
//     id: 5,
//     title: 'Colors, Backgrounds, and Gradients',
//     author: 'Eric A. Meyer',
//     available: true,
//     category: Category.CSS,
//     pages: 200,
//     markDamage:  (reason: string) => console.log(`Damage: ${ reason}`)
// };
//
// printBook(myBooks);

// myBooks.markDamage('missing bach coveredge');


// let logDamage: Logger = (reason) => console.log(`Damage: ${ reason}`);
// logDamage('missing bach coveredge');
//
//


// const favoriteAuthor: Author = {
//     name: 'Ann',
//     email : 'ex@fmvks',
//     numBooksPublished: 50,
// };

// const favoriteLibrarian: Librarian = {
//     name: 'Don',
//     email : 'ex@gmail',
//     department: 'main',
//     assistCustomer: custName => console.log(`CustName: ${custName}`)
// };

// const offer: any = {
//     book: {
//         title: 'Essential TypeScript'
//     }
// };

// console.log(offer.book?.getTitle?.());


// console.log(getBookProp(myBooks, 'old'))





// const ref: ReferenceItem = new ReferenceItem('MY TITLE', 2020) ;
// ref.printItem();
// ref.pablisher = ('super pablisher');
// console.log(ref.pablisher);

// const refBook: Encyclopedia = new Encyclopedia('title', 2020, 3);
// console.log(refBook.printItem())


// const refBook: Encyclopedia = new Encyclopedia('title', 2020, 3);
// console.log(refBook.printItem());
// refBook.printCitation()


// const favoriteLibrarian: Librarian = new UnivercityLibrarian();
// favoriteLibrarian.name = 'Ann'
// favoriteLibrarian.assistCustomer('Boris')
//
// const personBook : PersonBook = {
//     author: "Anna",
//     available: true,
//     category: Category.JavaScript,
//     email: "ann@gmail.cpm",
//     id: 1,
//     name: "Ann",
//     title: "IMTITLE"
// };
// console.log(personBook);
//
//
// interface A {
//     name : string
// }
//
// interface B {
//     name : string
// }
//
// type AB  = A & B;
// const a : AB = {
//     name: 'Ann'
// };


//06.03
// const refBook: RefBook = new RefBook('title', 2020, 3);
// console.log(refBook.printItem());
// refBook.printCitation();


//06.05
// const flag = true;
// if (flag) {
//     import('./classes').then(module => {
//         const reader = new module.Reader();
//         reader.name = 'Nataliia';
//         console.log(reader)
//     }).catch(e => console.log(e))
// }


//07.01
// const inventory: Array<Book> = [
//     {
//         id: 1,
//         title: 'Refactoring JavaScript',
//         category: Category.JavaScript,
//         author: 'Evan Burchard',
//         available: true
//     },
//     {
//         id: 2,
//         title: 'JavaScript Testing',
//         category: Category.JavaScript,
//         author: 'Liang Yuxian Eugene',
//         available: false
//     },
//     {
//         id: 3,
//         title: 'CSS Secrets',
//         category: Category.CSS,
//         author: 'Lea Verou',
//         available: true
//     },
//     {
//         id: 4,
//         title: 'Mastering JavaScript Object-Oriented Programming',
//         category: Category.JavaScript,
//         author: 'Andrea Chiarelli',
//         available: true
//     }
// ];
//
// console.log(purge(inventory));
// const res = purge<number>([1, 3, 5]);
// console.log(res)


//07.02
// const inventory: Array<Book> = [
//     {
//         id: 1,
//         title: 'Refactoring JavaScript',
//         category: Category.JavaScript,
//         author: 'Evan Burchard',
//         available: true
//     },
//     {
//         id: 2,
//         title: 'JavaScript Testing',
//         category: Category.JavaScript,
//         author: 'Liang Yuxian Eugene',
//         available: false
//     },
//     {
//         id: 3,
//         title: 'CSS Secrets',
//         category: Category.CSS,
//         author: 'Lea Verou',
//         available: true
//     },
//     {
//         id: 4,
//         title: 'Mastering JavaScript Object-Oriented Programming',
//         category: Category.JavaScript,
//         author: 'Andrea Chiarelli',
//         available: true
//     }
// ];
//
// const bookShelf: Shelf<Book> = new Shelf<Book>();
// inventory.forEach(book => bookShelf.add(book));
// console.log(bookShelf.getFirst().title)

//
// const magazines: Array<Magazine> = [
//     { title: 'Programming Language Monthly', publisher: 'Code Mags' },
//     { title: 'Literary Fiction Quarterly', publisher: 'College Press' },
//     { title: 'Five Points', publisher: 'GSU' }
// ];
//
// const magazineShelf: Shelf<Magazine> = new Shelf<Magazine>();
// magazines.forEach(item => magazineShelf.add(item));
// console.log(magazineShelf.getFirst().title);
//
//
// //07.03
// magazineShelf.printTitles();
// console.log(magazineShelf.find('Five Points'))


//07.04
// const a: BookRequiredFields = {
//     id: 1,
//     title: 'title',
//     category: Category.JavaScript,
//     markDamage: null,
//     pages: 200,
//     author: "Me",
//     available: true
// };
//
// const b: UpdatedBook = {
//     title :" non required title"
// };

// const params: Parameters<CreateCustomerFunctionType> = ['Nataliia'];
// createCustomer(...params);

//08.01, 08.02
// const lib = new UnivercityLibrarian();
// console.log(lib);
// lib['age'] = 15;
// lib.name = 'Ann';
// lib['printLibrarian']();

//08.03
// const lib = new UnivercityLibrarian();
// lib.assistFaculty = null;
// lib.teachCommunity = null;

//08.04
// const encyclopedia = new RefBook('no titile', 2020, 3);
// encyclopedia.printItem();

//08.05
// const lib = new UnivercityLibrarian();
// lib.name = 'Ann';
// lib.assistCustomer('Boris');
// console.log(lib);


//08.06
// const lib = new UnivercityLibrarian();
// lib.name = 'Ann';
// console.log(lib.name);
// lib.assistCustomer('Boris');

//08.07
// const en = new RefBook('title', 2020, 3);
// en.copies = 10

//09.01
// console.log('start');
// getBooksByCategory(Category.Software, logCategorySearch);
// console.log('finish');

//09.02
// console.log('start');
// getBooksByCategoryPromise(Category.JavaScript)
//     .then(titles => {
//         console.log(titles);
//         return titles.length;
//     })
//     .then(length => console.log(`number of book ${length}`))
//     .catch(reason => console.log(reason))
//     .finally(() => console.log('end of promise'));
// console.log('finish');


//09.03
console.log('start');
logSearchResult(Category.Software).catch(reason => console.log(reason));
console.log('finish');
