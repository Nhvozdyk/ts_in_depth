import {timeout} from "../decorators";

export abstract class ReferenceItem {
    private _publisher: string;

    get pablisher(): string {
        return this._publisher.toUpperCase();
    }
    set pablisher(newPablisher)  {
        this._publisher = newPablisher
    }
    constructor(public title: string, protected year: number) {
        console.log(`Creating a new ReferenceItem..`);
    }
    @timeout(10000)
    printItem() : void {
        console.log(`${this.title} was published in ${this.year}`);
        console.log(`Department: ${ReferenceItem.department}`)
    }
    static department: string = 'MY department';
    abstract printCitation() : void;
}
