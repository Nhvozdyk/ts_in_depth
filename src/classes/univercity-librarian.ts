import * as Interfaces from "../interfaces";
import {format, logger, logMethod, logParam, sealed, writable} from "../decorators";
// @sealed('UnivercityLibrarian')
// @logger
export class UnivercityLibrarian implements Interfaces.Librarian {
    @format()
    name: string;
    email: string;
    department: string;
    // @logMethod
    assistCustomer (customerName: string): void {
        console.log(`${this.name} assist customer: ${customerName}`)
    }
    // @writable(true)
   assistFaculty () {
        console.log(`Assisting faculty`)
   }
    // @writable(false)
   teachCommunity () {
        console.log(`Teach comm`);
   }
}
